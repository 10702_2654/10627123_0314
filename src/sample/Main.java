package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        /*
        FlowPane root = new FlowPane();
        Button butStart = new Button("OK");
        Button butCancel = new Button("Cancel");
        Button butPlus = new Button("+");
        Button butSub = new Button("-");
        Button butMul = new Button("*");
        Button butExc = new Button("/");

        butStart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
              System.out.print("Start");
            }
        });
        butCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.print("Cancel");
            }
        });




        root.getChildren().addAll(butStart,butCancel,butPlus,but);
        */

        primaryStage.setTitle("計算機T");
        primaryStage.setScene(new Scene(root, 600, 450));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
