package sample;

//import com.sun.java.util.jar.pack.Instruction;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import java.text.DecimalFormat;
import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import sun.invoke.empty.Empty;

import javax.swing.*;
import java.awt.*;
import java.text.Format;

public class Controller {

    public Label monitor;
    public Button butOne;
    public Button butTwo;
    public Button butThree;
    public Button butFour;
    public Button butFive;
    public Button butSix;
    public Button butSeven;
    public Button butEight;
    public Button butNine;
    public Button butZero;
    public Button butPlus;
    public Button butSubtrat;
    public Button butMultiply;
    public Button butExcept;
    public Button butPoint;
    public Button butAllClear;
    public Button butClear;
    public Button butResult;

    public Double input=0.0;
    public Double number=0.0;
    public String lastsingle= "";
    boolean N=true;


    public void inputNumber(ActionEvent actionEvent) {
        if(N==false)
            monitor.setText("");
        if(lastsingle.contentEquals("="))
            number=0.0;
        Button button = (Button) actionEvent.getSource();
        String number = monitor.getText();
            monitor.setText(number.concat(button.getText()));
            System.out.println(monitor.getText());
        N=true;
    }

    public void doCompute(ActionEvent actionEvent) {


        Button button = (Button) actionEvent.getSource();
        if(monitor.getText().isEmpty())
            input=0.0;
        else
            input = Double.parseDouble(monitor.getText());

        System.out.println(input);
        double tmp = input;

        if(number==0){
            number=tmp;
        }
        else{
            if(button.getText().contentEquals("c")){
                tmp=0;
            }
            else if(button.getText().contentEquals("AC")){
                number=0.0;
                lastsingle="";
            }


            if(N&&!button.getText().contentEquals("=")){
                switch (lastsingle){
                    case "AC":
                        lastsingle="AC";
                        break;
                    case "+":
                        number+=tmp;
                        lastsingle="+";
                        break;
                    case "-":
                        number-=tmp;
                        lastsingle="-";
                        break;
                    case "*":
                        number*=tmp;
                        lastsingle="*";
                        break;
                    case "/":
                        number/=tmp;
                        lastsingle="/";
                        break;

                }
            }


            DecimalFormat decimalFormat = new DecimalFormat("###################.###########");

            if(button.getText().contentEquals("=")&&N){
                if(lastsingle.contentEquals("="))
                    number=0.0;
                else if(lastsingle.contentEquals("+"))
                    number+=tmp;
                else if(lastsingle.contentEquals("-"))
                    number-=tmp;
                else if(lastsingle.contentEquals("*"))
                    number*=tmp;
                else if(lastsingle.contentEquals("/"))
                    number/=tmp;
                lastsingle=button.getText();
                monitor.setText(decimalFormat.format(number));
            }
            else if(button.getText().contentEquals("c")){
                monitor.setText("");
            }
            else if(button.getText().contentEquals("AC")){
                monitor.setText("");
            }
            else
                monitor.setText(decimalFormat.format(number));


        }



        if(!lastsingle.contentEquals("c"))
            lastsingle=button.getText();
        System.out.println(number);
        N=false;
        //lastsingle=button.getText();


    }
}
